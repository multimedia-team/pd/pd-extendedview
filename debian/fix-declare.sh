#!/bin/sh

perl -0777 -i -pe 's/(#X( obj [0-9]+ [0-9]+)? declare) [^;]*;/\1 -stdpath extendedview -stdpath extendedview\/ev-main -stdpath extendedview\/ev-in -stdpath extendedview\/ev-pano -stdpath extendedview\/ev-pro -stdpath extendedview\/ev-glsl -stdlib Gem -stdpath gil -stdpath kollabs;/igs' $@
